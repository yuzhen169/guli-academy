package com.atguigu.servicestatistics.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author yuzhen
 */
@Component
@FeignClient("service-ucenter")
public interface ServiceUcenterFeign {

    //查询某一天注册人数
    @GetMapping("/serviceucenter/member/countRegister/{day}")
    R countRegister(@PathVariable("day") String day);

}
