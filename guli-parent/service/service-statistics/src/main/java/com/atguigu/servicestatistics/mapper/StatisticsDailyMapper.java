package com.atguigu.servicestatistics.mapper;


import com.atguigu.servicestatistics.entity.StatisticsDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网站统计日数据 Mapper 接口
 * </p>
 *
 * @since 2020-03-14
 */
public interface StatisticsDailyMapper extends BaseMapper<StatisticsDaily> {

}
