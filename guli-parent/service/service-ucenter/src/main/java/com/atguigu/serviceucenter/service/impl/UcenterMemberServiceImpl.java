package com.atguigu.serviceucenter.service.impl;

import com.atguigu.common.enums.ResponseEnum;
import com.atguigu.common.exception.Assert;
import com.atguigu.common.utils.JwtUtils;
import com.atguigu.serviceucenter.entity.UcenterMember;
import com.atguigu.serviceucenter.entity.vo.RegisterVo;
import com.atguigu.serviceucenter.mapper.UcenterMemberMapper;
import com.atguigu.serviceucenter.service.UcenterMemberService;
import com.atguigu.common.utils.MD5;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 * @author testjava
 * @since 2020-03-09
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /**
     * 登录的方法
     */
    @Override
    public String login(UcenterMember member) {
        // 获取登录手机号和密码
        String mobile = member.getMobile();
        String password = member.getPassword();

        // 手机号和密码非空判断
        Assert.notEmpty(mobile, ResponseEnum.LOGIN_FAIL);
        Assert.notEmpty(password, ResponseEnum.LOGIN_FAIL);

        // 判断手机号是否正确
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", mobile);
        UcenterMember mobileMember = baseMapper.selectOne(wrapper);
        // 判断查询对象是否为空
        Assert.notNull(mobileMember, ResponseEnum.LOGIN_FAIL);

        // 判断密码
        // 因为存储到数据库密码肯定加密的
        // 把输入的密码进行加密，再和数据库密码进行比较
        // 加密方式 MD5
        Assert.equals(MD5.encrypt(password), mobileMember.getPassword(), ResponseEnum.LOGIN_FAIL);

        // 判断用户是否禁用
        Assert.isTrue(!mobileMember.getIsDisabled(), ResponseEnum.LOGIN_FAIL);

        // 登录成功
        // 生成token字符串，使用jwt工具类
        return JwtUtils.getJwtToken(mobileMember.getId(), mobileMember.getNickname());
    }

    /**
     * 注册的方法
     */
    @Override
    public void register(RegisterVo registerVo) {
        // 获取注册的数据
        String code = registerVo.getCode(); //验证码
        String mobile = registerVo.getMobile(); //手机号
        String nickname = registerVo.getNickname(); //昵称
        String password = registerVo.getPassword(); //密码

        // 非空判断
        Assert.notEmpty(mobile, ResponseEnum.REGISTER_FAIL);
        Assert.notEmpty(password, ResponseEnum.REGISTER_FAIL);
        Assert.notEmpty(nickname, ResponseEnum.REGISTER_FAIL);

        // 判断验证码
        // 获取redis验证码
        String redisCode = redisTemplate.opsForValue().get(mobile);
        Assert.equals(code, redisCode, ResponseEnum.REGISTER_FAIL);

        // 判断手机号是否重复，表里面存在相同手机号不进行添加
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", mobile);
        Integer count = baseMapper.selectCount(wrapper);
        Assert.equals(count, 0, ResponseEnum.REGISTER_FAIL);

        // 数据添加数据库中
        UcenterMember member = new UcenterMember();
        member.setMobile(mobile);
        member.setNickname(nickname);
        member.setPassword(MD5.encrypt(password));//密码需要加密的
        member.setIsDisabled(false);//用户不禁用
        member.setAvatar("http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132");
        baseMapper.insert(member);
    }

    //根据openid判断
    @Override
    public UcenterMember getOpenIdMember(String openid) {
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("openid", openid);
        UcenterMember member = baseMapper.selectOne(wrapper);
        return member;
    }

    @Override
    public Integer countRegisterDay(String day) {
        return baseMapper.countRegisterDay(day);
    }
}
