package com.atguigu.serviceoss.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.atguigu.serviceoss.service.OssService;
import com.atguigu.serviceoss.util.OssProperties;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * @author yuzhen
 */
@Service
public class OssServiceImpl implements OssService {

    @Override
    public String uploadOssFile(MultipartFile file) {
        // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
        String endpoint = OssProperties.ENDPOINT;
        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
        String accessKeyId = OssProperties.KEY_ID;
        String accessKeySecret = OssProperties.KEY_SECRET;
        String bucketName = OssProperties.BUCKET_NAME;

        try {
            // 创建OSSClient实例。
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

            // 填写本地文件的完整路径。如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件流。
            InputStream inputStream = file.getInputStream();

            String fileName = file.getOriginalFilename();

            String uuid = UUID.randomUUID().toString().replace("-", "");

            String dataPath = new DateTime()
                                .toString("yyyy/MM/dd")
                                .concat("/")
                                .concat(uuid)
                                .concat(fileName);

            // 依次填写Bucket名称（例如examplebucket）和Object完整路径（例如exampledir/exampleobject.txt）。Object完整路径中不能包含Bucket名称。
            ossClient.putObject(bucketName, dataPath, inputStream);

            // 关闭OSSClient。
            ossClient.shutdown();

            return "https://".concat(bucketName)
                    .concat(".")
                    .concat(endpoint)
                    .concat("/")
                    .concat(dataPath);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
