package com.atguigu.serviceoss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author yuzhen
 */
public interface OssService {

    String uploadOssFile(MultipartFile file);

}
