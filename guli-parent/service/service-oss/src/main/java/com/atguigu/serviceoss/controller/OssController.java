package com.atguigu.serviceoss.controller;

import com.atguigu.common.utils.R;
import com.atguigu.serviceoss.service.OssService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author yuzhen
 */
@Api(tags = "OSS对象存储")
@RestController
@RequestMapping("/oss")
public class OssController {

    @Autowired
    private OssService ossService;

    @ApiOperation("上传文件")
    @PostMapping
    public R uploadOssFile(MultipartFile file) {
        String url = ossService.uploadOssFile(file);
        return R.ok().data("url", url);
    }

}
