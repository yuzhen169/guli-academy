package com.atguigu.servicevod.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author yuzhen
 */
public interface VodService {

    String uploadVideoAly(MultipartFile file);

    void removeMoreAlyVideo(List<String> videoIdList);

}
