package com.atguigu.vodtest;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.vod.model.v20170321.GetPlayInfoRequest;
import com.aliyuncs.vod.model.v20170321.GetPlayInfoResponse;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;

import java.util.List;

/**
 * @author yuzhen
 */
public class TestVod {

    public static void main(String[] args) throws Exception {
        getPlayUrl();
    }

    //1 根据视频iD获取视频播放凭证
    public static void getPlayAuth() throws Exception{
        DefaultAcsClient client = InitObject.initVodClient(
                "LTAI5tHLsNEffDvBoYnscKbN",
                "kmrMDpqPEdAUXvY6klO0ioLJPZoae7");

        GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
        GetVideoPlayAuthResponse response = new GetVideoPlayAuthResponse();

        request.setVideoId("b70be77270f64d2b860e154040b8f48e");

        response = client.getAcsResponse(request);
        System.out.println("playAuth:"+response.getPlayAuth());
    }

    // 1 根据视频iD获取视频播放地址
    public static void getPlayUrl() throws Exception{
        // 创建初始化对象
        DefaultAcsClient client = InitObject.initVodClient(
                "LTAI5tHLsNEffDvBoYnscKbN",
                "kmrMDpqPEdAUXvY6klO0ioLJPZoae7");

        // 创建获取视频地址request和response
        GetPlayInfoRequest request = new GetPlayInfoRequest();
        GetPlayInfoResponse response = new GetPlayInfoResponse();

        // 向request对象里面设置视频id
        request.setVideoId("b70be77270f64d2b860e154040b8f48e");

        // 调用初始化对象里面的方法，传递request，获取数据
        response = client.getAcsResponse(request);

        List<GetPlayInfoResponse.PlayInfo> playInfoList = response.getPlayInfoList();
        // 播放地址
        for (GetPlayInfoResponse.PlayInfo playInfo : playInfoList) {
            System.out.print("PlayInfo.PlayURL = " + playInfo.getPlayURL() + "\n");
        }
        // Base信息
        System.out.print("VideoBase.Title = " + response.getVideoBase().getTitle() + "\n");
    }

}
