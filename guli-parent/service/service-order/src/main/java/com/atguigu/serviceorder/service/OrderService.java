package com.atguigu.serviceorder.service;

import com.atguigu.serviceorder.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 订单 服务类
 * @author yuzhen
 */
public interface OrderService extends IService<Order> {

    // 1 生成订单的方法
    String createOrders(String courseId, String memberId);

}
