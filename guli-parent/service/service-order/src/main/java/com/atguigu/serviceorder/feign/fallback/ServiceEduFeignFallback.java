package com.atguigu.serviceorder.feign.fallback;

import com.atguigu.common.vo.CourseWebOrderVo;
import com.atguigu.serviceorder.feign.ServiceEduFeign;

/**
 * @author yuzhen
 */
public class ServiceEduFeignFallback implements ServiceEduFeign {

    @Override
    public CourseWebOrderVo getCourseInfoOrder(String id) {
        return null;
    }

}
