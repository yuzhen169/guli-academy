package com.atguigu.serviceorder.feign;

import com.atguigu.common.vo.CourseWebOrderVo;
import com.atguigu.serviceorder.feign.fallback.ServiceEduFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author yuzhen
 */
@Component
@FeignClient(name = "service-edu", fallback = ServiceEduFeignFallback.class)
public interface ServiceEduFeign {

    // 根据课程id查询课程信息
    @PostMapping("/serviceedu/courseFront/getCourseInfoOrder/{id}")
    CourseWebOrderVo getCourseInfoOrder(@PathVariable("id") String id);

}
