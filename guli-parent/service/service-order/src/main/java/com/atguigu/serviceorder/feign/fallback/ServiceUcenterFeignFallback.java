package com.atguigu.serviceorder.feign.fallback;

import com.atguigu.common.vo.UcenterMemberOrderVo;
import com.atguigu.serviceorder.feign.ServiceUcenterFeign;

/**
 * @author yuzhen
 */
public class ServiceUcenterFeignFallback implements ServiceUcenterFeign {

    @Override
    public UcenterMemberOrderVo getUserInfoOrder(String id) {
        return null;
    }

}
