package com.atguigu.serviceorder.feign;

import com.atguigu.common.vo.UcenterMemberOrderVo;
import com.atguigu.serviceorder.feign.fallback.ServiceUcenterFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(name = "service-ucenter", fallback = ServiceUcenterFeignFallback.class)
public interface ServiceUcenterFeign {

    // 根据用户id获取用户信息
    @GetMapping("/serviceucenter/member/getUcenterMemberById/{id}")
    UcenterMemberOrderVo getUserInfoOrder(@PathVariable("id") String id);

}
