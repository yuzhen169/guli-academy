package com.atguigu.servicemsm.service;

import java.util.Map;

/**
 * @author yuzhen
 */
public interface MsmService {

    boolean send(Map<String, Object> param, String phone);

}
