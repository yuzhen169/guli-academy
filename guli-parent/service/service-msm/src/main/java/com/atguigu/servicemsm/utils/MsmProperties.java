package com.atguigu.servicemsm.utils;

import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author yuzhen
 */
@Data
@Component
@ConfigurationProperties(prefix = "aliyun.msm")
public class MsmProperties implements InitializingBean {

    private String keyId;
    private String keySecret;

    public static String KEY_ID;
    public static String KEY_SECRET;

    //当私有成员被赋值后，此方法自动被调用，从而初始化常量
    @Override
    public void afterPropertiesSet() throws Exception {
        KEY_ID = keyId;
        KEY_SECRET = keySecret;
    }

}
