package com.atguigu.servicecms.mapper;

import com.atguigu.servicecms.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 首页banner表 Mapper 接口
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
