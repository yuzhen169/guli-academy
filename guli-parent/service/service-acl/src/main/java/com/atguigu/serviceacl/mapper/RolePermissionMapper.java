package com.atguigu.serviceacl.mapper;


import com.atguigu.serviceacl.entity.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限 Mapper 接口
 * </p>
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
