package com.atguigu.serviceacl.service;

import com.atguigu.serviceacl.entity.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限 服务类
 * </p>
 */
public interface RolePermissionService extends IService<RolePermission> {

}
