package com.atguigu.serviceacl.mapper;

import com.atguigu.serviceacl.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
