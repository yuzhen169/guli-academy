package com.atguigu.serviceacl.service;

import com.atguigu.serviceacl.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 服务类
 * @author yuzhen
 */
public interface UserRoleService extends IService<UserRole> {

}