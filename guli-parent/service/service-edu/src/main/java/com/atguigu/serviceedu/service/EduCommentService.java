package com.atguigu.serviceedu.service;

import com.atguigu.serviceedu.entity.EduComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评论 服务类
 * </p>
 *
 * @author yzh
 * @since 2022-03-06
 */
public interface EduCommentService extends IService<EduComment> {

}
