package com.atguigu.serviceedu.controller;


import com.atguigu.common.utils.R;
import com.atguigu.serviceedu.entity.EduSubject;
import com.atguigu.serviceedu.service.EduSubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 * @author yzh
 * @since 2022-01-09
 */
@Api(tags = "课程科目")
@RestController
@RequestMapping("/subject")
public class EduSubjectController {

    @Autowired
    private EduSubjectService subjectService;

    @ApiOperation("上传课程Excel数据")
    @PostMapping
    public R uploadSubjectExcel(MultipartFile file) {
        subjectService.dealWithSubjectExcelData(file, subjectService);
        return R.ok();
    }

    @ApiOperation("获取课程分类树形列表")
    @GetMapping
    public R getSubjectTreeList() {
        List<EduSubject> list = subjectService.getSubjectTreeListByRecursionMap();
        return R.ok().data("list", list);
    }

}

