package com.atguigu.serviceedu.mapper;

import com.atguigu.serviceedu.entity.EduTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author yzh
 * @since 2022-01-02
 */
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
