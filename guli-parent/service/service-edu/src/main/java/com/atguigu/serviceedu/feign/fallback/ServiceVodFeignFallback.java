package com.atguigu.serviceedu.feign.fallback;

import com.atguigu.common.utils.R;
import com.atguigu.serviceedu.feign.ServiceVodFeign;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author yuzhen
 */
@Component
public class ServiceVodFeignFallback implements ServiceVodFeign {
    @Override
    public R removeAlyVideo(String id) {
        return R.error().message("删除视频出错了");
    }

    @Override
    public R deleteBatch(List<String> videoIdList) {
        return R.error().message("删除多个视频出错了");
    }
}
