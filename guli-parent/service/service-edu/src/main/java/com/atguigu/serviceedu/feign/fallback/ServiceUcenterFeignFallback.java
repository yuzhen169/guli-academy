package com.atguigu.serviceedu.feign.fallback;

import com.atguigu.serviceedu.entity.vo.UcenterMemberVo;
import com.atguigu.serviceedu.feign.ServiceUcenterFeign;

/**
 * @author yuzhen
 */
public class ServiceUcenterFeignFallback implements ServiceUcenterFeign {

    @Override
    public UcenterMemberVo getUcenterInfo(String memberId) {
        return null;
    }

}
