package com.atguigu.serviceedu.mapper;

import com.atguigu.serviceedu.entity.EduComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论 Mapper 接口
 * </p>
 *
 * @author yzh
 * @since 2022-03-06
 */
public interface EduCommentMapper extends BaseMapper<EduComment> {

}
