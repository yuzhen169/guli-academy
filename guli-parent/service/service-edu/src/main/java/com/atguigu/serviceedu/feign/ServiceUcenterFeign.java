package com.atguigu.serviceedu.feign;

import com.atguigu.serviceedu.entity.vo.UcenterMemberVo;
import com.atguigu.serviceedu.feign.fallback.ServiceUcenterFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author yuzhen
 */
@Component
@FeignClient(name = "service-ucenter", fallback = ServiceUcenterFeignFallback.class)
public interface ServiceUcenterFeign {

    // 根据用户id获取用户信息
    @GetMapping("/serviceucenter/member/getUcenterMemberById/{memberId}")
    UcenterMemberVo getUcenterInfo(@PathVariable("memberId") String memberId);

}
