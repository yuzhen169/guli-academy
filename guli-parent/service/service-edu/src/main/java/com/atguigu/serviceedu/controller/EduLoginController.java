package com.atguigu.serviceedu.controller;

import com.atguigu.common.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * @author yuzhen
 */
@Api(tags = "用户模块")
@RestController
@RequestMapping("/user")
public class EduLoginController {

    //login
    @ApiOperation("用户登录")
    @PostMapping("login")
    public R login() {
        return R.ok().data("token", "admin");
    }

    //info
    @ApiOperation("获取用户")
    @GetMapping("info")
    public R info() {
        return R.ok().data("roles", "[admin]").data("name", "admin").data("avatar", "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
    }
}

