package com.atguigu.serviceedu.mapper;

import com.atguigu.serviceedu.entity.EduChapter;
import com.atguigu.serviceedu.entity.vo.ChapterVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 章节 Mapper 接口
 * </p>
 *
 * @author yzh
 * @since 2022-01-15
 */
@Repository
public interface EduChapterMapper extends BaseMapper<EduChapter> {

    List<ChapterVo> getChapterVideoByCourseId(@Param("courseId") String courseId);

}
