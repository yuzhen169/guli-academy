package com.atguigu.serviceedu.feign;

import com.atguigu.serviceedu.feign.fallback.ServiceOrderFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author yuzhen
 */
@Component
@FeignClient(name = "service-order", fallback = ServiceOrderFeignFallback.class)
public interface ServiceOrderFeign {

    // 根据课程id和用户id查询订单表中订单状态
    @GetMapping("/serviceorder/order/isBuyCourse/{courseId}/{memberId}")
    Boolean isBuyCourse(@PathVariable("courseId") String courseId, @PathVariable("memberId") String memberId);

}
