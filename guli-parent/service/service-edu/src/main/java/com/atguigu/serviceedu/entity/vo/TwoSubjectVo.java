package com.atguigu.serviceedu.entity.vo;

import lombok.Data;

/**
 * @author yuzhen
 */
@Data
public class TwoSubjectVo {

    private String id;
    private String title;
    private String parentId;

}
