package com.atguigu.serviceedu.controller;


import com.atguigu.common.enums.ResponseEnum;
import com.atguigu.common.utils.R;
import com.atguigu.serviceedu.entity.EduCourse;
import com.atguigu.serviceedu.entity.form.CourseInfoForm;
import com.atguigu.serviceedu.entity.query.CourseQuery;
import com.atguigu.serviceedu.entity.vo.CourseInfoVo;
import com.atguigu.serviceedu.entity.vo.CoursePublishVo;
import com.atguigu.serviceedu.service.EduCourseService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 * @author yzh
 * @since 2022-01-15
 */
@Api(tags = "课程管理")
@RestController
@RequestMapping("/course")
public class EduCourseController {

    @Autowired
    private EduCourseService courseService;

    @ApiOperation("分页查询课程")
    @PostMapping("{page}/{limit}")
    public R pageQuery(@ApiParam(name = "page", value = "当前页码", required = true)
                       @PathVariable Long page,
                       @ApiParam(name = "limit", value = "每页记录数", required = true)
                       @PathVariable Long limit,
                       @ApiParam(name = "courseQuery", value = "查询对象")
                       @RequestBody(required = false) CourseQuery courseQuery) {
        Page<EduCourse> pageParam = new Page<>(page, limit);
        courseService.pageQuery(pageParam, courseQuery);

        List<EduCourse> records = pageParam.getRecords();
        long total = pageParam.getTotal();

        return R.ok().data("total", total).data("list", records);
    }

    @ApiOperation("根据课程id查询课程基本信息")
    @GetMapping("getCourseInfoById/{id}")
    public R getCourseInfoById(
            @ApiParam(name = "id", value = "课程ID", required = true)
            @PathVariable String id) {
        CourseInfoVo courseInfoVo = courseService.getCourseInfoById(id);
        return R.ok().data("courseInfo", courseInfoVo);
    }

    @ApiOperation("根据课程id查询课程确认信息")
    @GetMapping("getPublishCourseInfo/{id}")
    public R getPublishCourseInfo(
            @ApiParam(name = "id", value = "课程ID", required = true)
            @PathVariable String id) {
        CoursePublishVo coursePublishVo = courseService.publishCourseInfo(id);
        return R.ok().data("publishCourse", coursePublishVo);
    }

    @ApiOperation("添加课程基本信息的方法")
    @PostMapping
    public R addCourseInfo(
            @ApiParam(name = "courseInfoForm", value = "课程基本信息", required = true)
            @RequestBody CourseInfoForm courseInfoForm) {
        // 返回添加之后课程id，为了后面添加大纲使用
        String id = courseService.saveCourseInfo(courseInfoForm);
        return StringUtils.isNotBlank(id) ? R.ok().data("courseId", id) : R.error(ResponseEnum.ADD_FAIL);
    }

    @ApiOperation("修改课程信息")
    @PutMapping
    public R updateCourseInfo(
            @ApiParam(name = "courseInfoForm", value = "课程基本信息", required = true)
            @RequestBody CourseInfoForm courseInfoForm) {
        return courseService.updateCourseInfo(courseInfoForm) ? R.ok(ResponseEnum.UPDATE_SUCCESS)
                : R.error(ResponseEnum.UPDATE_FAIL);
    }

    @ApiOperation("课程最终发布")
    @PutMapping("publishCourse/{id}")
    public R publishCourse(
            @ApiParam(name = "id", value = "课程ID", required = true)
            @PathVariable String id) {
        return courseService.publishCourse(id) ? R.ok(ResponseEnum.PUBLISH_COURSE_SUCCESS)
                : R.ok(ResponseEnum.PUBLISH_COURSE_ERROR);
    }

    @ApiOperation("删除课程")
    @DeleteMapping("{courseId}")
    public R deleteCourse(
            @ApiParam(name = "id", value = "课程ID", required = true)
            @PathVariable String courseId) {
        return courseService.removeCourse(courseId) ? R.ok(ResponseEnum.DELETE_SUCCESS)
                : R.error(ResponseEnum.DELETE_FAIL);
    }

}

