package com.atguigu.serviceedu.feign;

import com.atguigu.common.utils.R;
import com.atguigu.serviceedu.feign.fallback.ServiceVodFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author yuzhen
 */
@FeignClient(name = "service-vod", fallback = ServiceVodFeignFallback.class)
@Component
public interface ServiceVodFeign {

    /**
     * 定义调用的方法路径
     * 根据视频id删除阿里云视频
     */
    @DeleteMapping("/servicevod/vod/removeAlyVideo/{id}")
    R removeAlyVideo(@PathVariable("id") String id);

    /**
     * 定义调用删除多个视频的方法
     * 删除多个阿里云视频的方法
     * 参数多个视频id  List videoIdList
     */
    @DeleteMapping("/servicevod/vod/deleteBatch")
    R deleteBatch(@RequestParam("videoIdList") List<String> videoIdList);

}
