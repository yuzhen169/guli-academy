package com.atguigu.serviceedu.entity.query;

import lombok.Data;

/**
 * @author yuzhen
 */
@Data
public class CourseQuery {

    private String title;
    private String status;

}
