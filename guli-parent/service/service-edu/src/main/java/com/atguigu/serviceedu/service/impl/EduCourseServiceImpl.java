package com.atguigu.serviceedu.service.impl;

import com.atguigu.common.enums.ResponseEnum;
import com.atguigu.common.exception.Assert;
import com.atguigu.serviceedu.entity.EduCourse;
import com.atguigu.serviceedu.entity.EduCourseDescription;
import com.atguigu.serviceedu.entity.form.CourseInfoForm;
import com.atguigu.serviceedu.entity.frontvo.CourseFrontVo;
import com.atguigu.serviceedu.entity.frontvo.CourseWebVo;
import com.atguigu.serviceedu.entity.query.CourseQuery;
import com.atguigu.serviceedu.entity.vo.CourseInfoVo;
import com.atguigu.serviceedu.entity.vo.CoursePublishVo;
import com.atguigu.serviceedu.mapper.EduCourseMapper;
import com.atguigu.serviceedu.service.EduChapterService;
import com.atguigu.serviceedu.service.EduCourseDescriptionService;
import com.atguigu.serviceedu.service.EduCourseService;
import com.atguigu.serviceedu.service.EduVideoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 * @author yzh
 * @since 2022-01-15
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {

    @Autowired
    private EduCourseDescriptionService courseDescriptionService;
    @Autowired
    private EduVideoService videoService;
    @Autowired
    private EduChapterService chapterService;

    @Transactional
    @Override
    public String saveCourseInfo(CourseInfoForm courseInfoForm) {
        EduCourse course = new EduCourse();
        BeanUtils.copyProperties(courseInfoForm, course);
        course.setStatus(EduCourse.COURSE_DRAFT);
        int insert = baseMapper.insert(course);
        Assert.notEquals(insert, 0, ResponseEnum.ADD_COURSE_INFO_ERROR);

        // 获取添加之后课程id
        String courseId = course.getId();

        // 2 向课程简介表添加课程简介
        // edu_course_description
        EduCourseDescription courseDescription = new EduCourseDescription();
        courseDescription.setDescription(courseInfoForm.getDescription());
        // 设置描述id就是课程id
        courseDescription.setId(courseId);
        boolean save = courseDescriptionService.save(courseDescription);
        Assert.isTrue(save, ResponseEnum.ADD_COURSE_DETAIL_INFO_ERROR);

        return courseId;
    }

    @Override
    public CourseInfoVo getCourseInfoById(String id) {
        CourseInfoVo courseInfoVo = new CourseInfoVo();
        EduCourse course = baseMapper.selectById(id);
        Assert.notNull(course, ResponseEnum.DATA_IS_NULL);
        BeanUtils.copyProperties(course, courseInfoVo);

        EduCourseDescription courseDescription = courseDescriptionService.getById(id);
        if (courseDescription != null) {
            courseInfoVo.setDescription(courseDescription.getDescription());
        }
        return courseInfoVo;
    }

    @Transactional
    @Override
    public boolean updateCourseInfo(CourseInfoForm courseInfoForm) {
        // 1 修改课程表
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoForm, eduCourse);
        int update = baseMapper.updateById(eduCourse);
        Assert.notEquals(update, 0, ResponseEnum.UPDATE_COURSE_INFO_ERROR);

        // 2 修改描述表
        EduCourseDescription description = new EduCourseDescription();
        description.setId(courseInfoForm.getId());
        description.setDescription(courseInfoForm.getDescription());
        return courseDescriptionService.updateById(description);
    }

    @Override
    public CoursePublishVo publishCourseInfo(String id) {
        return baseMapper.getPublishCourseInfo(id);
    }

    @Override
    public boolean publishCourse(String id) {
        EduCourse eduCourse = new EduCourse();
        eduCourse.setId(id);
        eduCourse.setStatus("Normal");//设置课程发布状态
        return this.updateById(eduCourse);
    }

    @Override
    public void pageQuery(Page<EduCourse> pageParam, CourseQuery courseQuery) {
        QueryWrapper queryWrapper = new QueryWrapper();

        if (courseQuery == null) {
            queryWrapper.orderByDesc("gmt_create");
            super.page(pageParam, queryWrapper);
            return;
        }

        if (StringUtils.isNotBlank(courseQuery.getTitle())) {
            queryWrapper.like("title", courseQuery.getTitle());
        }
        if (StringUtils.isNotBlank(courseQuery.getStatus())) {
            queryWrapper.eq("status", courseQuery.getStatus());
        }

        queryWrapper.orderByDesc("gmt_create");
        super.page(pageParam, queryWrapper);
    }

    @Override
    public boolean removeCourse(String courseId) {
        // 1 根据课程id删除小节
        videoService.removeVideoByCourseId(courseId);

        // 2 根据课程id删除章节
        chapterService.removeChapterByCourseId(courseId);

        // 3 根据课程id删除描述
        courseDescriptionService.removeById(courseId);

        // 4 根据课程id删除课程本身
        int result = baseMapper.deleteById(courseId);

        return result > 0;
    }

    @Override
    public Map<String, Object> getCourseFrontList(Page<EduCourse> pageParam, CourseFrontVo courseFrontVo) {
        // 2 根据讲师id查询所讲课程
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        // 判断条件值是否为空，不为空拼接
        if (!StringUtils.isEmpty(courseFrontVo.getSubjectParentId())) { //一级分类
            wrapper.eq("subject_parent_id", courseFrontVo.getSubjectParentId());
        }
        if (!StringUtils.isEmpty(courseFrontVo.getSubjectId())) { //二级分类
            wrapper.eq("subject_id", courseFrontVo.getSubjectId());
        }
        if (!StringUtils.isEmpty(courseFrontVo.getBuyCountSort())) { //关注度
            wrapper.orderByDesc("buy_count");
        }
        if (!StringUtils.isEmpty(courseFrontVo.getGmtCreateSort())) { //最新
            wrapper.orderByDesc("gmt_create");
        }

        if (!StringUtils.isEmpty(courseFrontVo.getPriceSort())) {//价格
            wrapper.orderByDesc("price");
        }

        baseMapper.selectPage(pageParam, wrapper);

        List<EduCourse> records = pageParam.getRecords();
        long current = pageParam.getCurrent();
        long pages = pageParam.getPages();
        long size = pageParam.getSize();
        long total = pageParam.getTotal();
        boolean hasNext = pageParam.hasNext();//下一页
        boolean hasPrevious = pageParam.hasPrevious();//上一页

        //把分页数据获取出来，放到map集合
        Map<String, Object> map = new HashMap<>();
        map.put("items", records);
        map.put("current", current);
        map.put("pages", pages);
        map.put("size", size);
        map.put("total", total);
        map.put("hasNext", hasNext);
        map.put("hasPrevious", hasPrevious);

        //map返回
        return map;
    }

    @Override
    public CourseWebVo getBaseCourseInfo(String courseId) {
        return baseMapper.getBaseCourseInfo(courseId);
    }

}
