package com.atguigu.serviceedu.feign.fallback;

import com.atguigu.serviceedu.feign.ServiceOrderFeign;
import org.springframework.stereotype.Component;

/**
 * @author yuzhen
 */
@Component
public class ServiceOrderFeignFallback implements ServiceOrderFeign {

    @Override
    public Boolean isBuyCourse(String courseId, String memberId) {
        return false;
    }

}
