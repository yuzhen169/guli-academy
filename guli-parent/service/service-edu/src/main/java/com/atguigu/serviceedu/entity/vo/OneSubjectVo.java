package com.atguigu.serviceedu.entity.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yuzhen
 */
@Data
public class OneSubjectVo {

    private String id;
    private String title;
    private String parentId;

    //一个一级分类有多个二级分类
    private List<TwoSubjectVo> children = new ArrayList<>();

}
