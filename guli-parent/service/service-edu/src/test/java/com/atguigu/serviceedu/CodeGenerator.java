package com.atguigu.serviceedu;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.junit.Test;

/**
 * @author yuzhen
 */
public class CodeGenerator {

    @Test
    public void test() {
        // 1.创建代码生成器
        AutoGenerator generator = new AutoGenerator();

        // 2.全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        System.out.println(projectPath);
        gc.setOutputDir(projectPath + "/src/main/java");
        gc.setAuthor("yzh");
        // 生成后是否打开资源管理器
        gc.setOpen(false);
        // 重新生成时文件是否覆盖
        gc.setFileOverride(false);
         /*
         * mp生成service层代码，默认接口名称第一个字母有 I
         * UcenterService
         * 去掉Service接口的首字母I
         */
        gc.setServiceName("%sService");
        gc.setIdType(IdType.ID_WORKER_STR); //主键策略
        gc.setDateType(DateType.ONLY_DATE);//定义生成的实体类中日期类型
        gc.setSwagger2(true);//开启Swagger2模式
        generator.setGlobalConfig(gc);

        // 3、数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://127.0.0.1:3306/guli?serverTimezone=GMT%2B8");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("yuzhen");
        dsc.setDbType(DbType.MYSQL);
        generator.setDataSource(dsc);

        // 4、包配置
        PackageConfig pc = new PackageConfig();
        // 模块名
        pc.setModuleName("serviceedu");
        pc.setParent("com.atguigu");
        pc.setController("controller");
        pc.setService("service");
        pc.setMapper("mapper");
        generator.setPackageInfo(pc);

        // 5、策略配置
        StrategyConfig sc = new StrategyConfig();
        //sc.setInclude("edu_comment", "edu_course", "edu_course_description", "edu_video");
        sc.setInclude("edu_comment");
        sc.setNaming(NamingStrategy.underline_to_camel);
        sc.setTablePrefix(pc.getModuleName() + "_");
        // 数据库表字段映射到实体的命名策略
        sc.setColumnNaming(NamingStrategy.underline_to_camel);
        // lombok 模型 @Accessors(chain = true) setter链式操作
        sc.setEntityLombokModel(true);
        // restful api风格控制器
        sc.setRestControllerStyle(true);
        // url中驼峰转连字符
        sc.setControllerMappingHyphenStyle(true);
        generator.setStrategy(sc);

        // 6、执行
        generator.execute();
    }

}
