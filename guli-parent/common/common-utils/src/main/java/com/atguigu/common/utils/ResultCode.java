package com.atguigu.common.utils;

/**
 * @author yuzhen
 */
public interface ResultCode {

    Integer SUCCESS = 20000;// 成功
    Integer FAIL = 20001;// 失败
    Integer ERROR = 20004;// 异常

    Integer IS_NULL = 30000;// 为空
    Integer IS_NOT_NULL = 30001;// 不为空

}
