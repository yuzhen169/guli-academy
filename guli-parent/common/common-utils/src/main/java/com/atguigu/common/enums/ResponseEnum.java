package com.atguigu.common.enums;

import com.atguigu.common.utils.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @author yuzhen
 */
@Getter
@AllArgsConstructor
@ToString
public enum ResponseEnum {

    NOT_DELETE_DATA(ResultCode.ERROR, "不能删除"),

    PUBLISH_COURSE_SUCCESS(ResultCode.SUCCESS, "课程发布成功"),
    PUBLISH_COURSE_ERROR(ResultCode.ERROR, "课程发布失败"),

    ADD_COURSE_INFO_ERROR(ResultCode.ERROR, "添加课程信息失败"),
    ADD_COURSE_DETAIL_INFO_ERROR(ResultCode.ERROR, "添加课程详情信息失败"),
    UPDATE_COURSE_INFO_ERROR(ResultCode.ERROR, "修改课程信息失败"),
    UPDATE_COURSE_DETAIL_INFO_ERROR(ResultCode.ERROR, "修改课程详情信息失败"),

    FILE_DATA_IS_NULL(ResultCode.IS_NULL,"文件数据为空"),
    DATA_IS_NULL(ResultCode.IS_NULL,"数据不存在"),

    ADD_SUCCESS(ResultCode.SUCCESS, "添加成功"),
    ADD_FAIL(ResultCode.FAIL, "添加失败"),
    UPDATE_SUCCESS(ResultCode.SUCCESS, "修改成功"),
    UPDATE_FAIL(ResultCode.FAIL, "修改失败"),
    DELETE_SUCCESS(ResultCode.SUCCESS, "删除成功"),
    DELETE_FAIL(ResultCode.FAIL, "删除失败"),

    REGISTER_FAIL(ResultCode.FAIL, "注册失败"),
    LOGIN_FAIL(ResultCode.FAIL, "登录失败"),

    SUCCESS(ResultCode.SUCCESS, "成功"),
    FAIL(ResultCode.FAIL, "失败"),
    ERROR(ResultCode.ERROR, "服务器内部错误");

    //响应状态码
    private Integer code;
    //响应信息
    private String message;

}
