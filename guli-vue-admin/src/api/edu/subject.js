import request from '@/utils/request';
const apiName = '/serviceedu';

export default {
    // 课程分类列表
    getSubjectTreeList() {
        return request({
            url: `${apiName}/subject`,
            method: 'get'
        })
    }
}
