import request from '@/utils/request'
const apiName = '/serviceedu';

export default {
  getCourseInfoById(id) {
    return request({
      url: `${apiName}/course/getCourseInfoById/${id}`,
      method: 'get'
    })
  },
  getPublihCourseInfo(id) {
    return request({
      url: `${apiName}/course/getPublishCourseInfo/${id}`,
      method: 'get'
    })
  },
  pageCourse(page, limit, courseQuery) {
    return request({
      url: `${apiName}/course/${page}/${limit}`,
      method: 'post',
      data: courseQuery
    })
  },
  addCourseInfo(courseInfo) {
    return request({
      url: `${apiName}/course`,
      method: 'post',
      data: courseInfo
    })
  },
  updateCourseInfo(courseInfo) {
    return request({
      url: `${apiName}/course`,
      method: 'put',
      data: courseInfo
    })
  },
  publishCourse(courseId) {
    return request({
      url: `${apiName}/course/publishCourse/${courseId}`,
      method: 'put'
    })
  },
  deleteCourse(courseId) {
    return request({
      url: `${apiName}/course/${courseId}`,
      method: 'delete'
    })
  }
    
}
