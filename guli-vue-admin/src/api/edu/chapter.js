import request from '@/utils/request'
const apiName = '/serviceedu';

export default {
  getChapterVideoList(courseId) {
    return request({
      url: `${apiName}/chapter/getChapterVideo/${courseId}`,
      method: 'get'
    })
  },
  // 根据id查询章节
  getChapter(chapterId) {
    return request({
      url: `${apiName}/chapter/getChapterInfo/${chapterId}`,
      method: 'get'
    })
  },
  // 添加章节
  addChapter(chapter) {
    return request({
      url: `${apiName}/chapter`,
      method: 'post',
      data: chapter
    })
  },
  // 修改章节
  updateChapter(chapter) {
    return request({
      url: `${apiName}/chapter`,
      method: 'put',
      data: chapter
    })
  },
  // 删除章节
  deleteChapter(chapterId) {
    return request({
      url: `${apiName}/chapter/${chapterId}`,
      method: 'delete'
    })
  },
}
