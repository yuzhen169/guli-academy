import request from "@/utils/request";
const apiName = '/serviceedu';

export default {
  pageTeacher(page, limit, teacherQuery) {
    return request({
      url: `${apiName}/teacher/${page}/${limit}`,
      method: "post",
      data: teacherQuery
    });
  },
  allTeacher(){
     return request({
      url: `${apiName}/teacher`,
      method: "get"
    });
  },
  getById(id) {
    return request({
      url: `${apiName}/teacher/${id}`,
      method: "get"
    });
  },
  deleteById(id) {
    return request({
      url: `${apiName}/teacher/${id}`,
      method: "delete"
    });
  },
  saveTeacher(teacher) {
    return request({
      url: `${apiName}/teacher`,
      method: "post",
      data: teacher
    });
  },
  updateTeacher(teacher) {
    return request({
      url: `${apiName}/teacher`,
      method: "put",
      data: teacher
    });
  }
};
