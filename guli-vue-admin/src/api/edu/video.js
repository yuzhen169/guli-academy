import request from '@/utils/request'
const apiName = '/serviceedu';
const servicevodApi = '/servicevod';

export default {
  getVideoById(videoId) {
    return request({
      url: `${apiName}/video/${videoId}`,
      method: 'get'
    })
  },

  // 添加小节
  addVideo(video) {
    return request({
      url: `${apiName}/video`,
      method: 'post',
      data: video
    })
  },

  // 修改小节
  updateVideo(video) {
    return request({
      url: `${apiName}/video`,
      method: 'put',
      data: video
    })
  },
  
  // 删除小节
  deleteVideo(id) {
    return request({
      url: `${apiName}/video/${id}`,
      method: 'delete'
    })
  },

  //删除视频
  deleteAliyunvod(id) {
    return request({
      url: `${servicevodApi}/vod/removeAlyVideo/${id}`,
      method: 'delete'
    })
  } 
}
