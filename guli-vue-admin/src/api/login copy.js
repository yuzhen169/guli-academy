import request from '@/utils/request';

export function login (username, password) {
  return request({
    url: '/serviceedu/user/login',
    method: 'post',
    data: {
      username,
      password
    }
  });
}

export function getInfo (token) {
  return request({
    url: '/serviceedu/user/info',
    method: 'get',
    params: { token }
  });
}

export function logout () {
  return request({
    url: '/serviceedu/user/logout',
    method: 'post'
  });
}

// 获取菜单权限数据
export function getMenu () {
  return request({
    url: '/admin/acl/index/menu',
    method: 'get'
  });
}
