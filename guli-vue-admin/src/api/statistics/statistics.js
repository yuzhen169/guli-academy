
import request from '@/utils/request';

export default {
  createStatistics (day) {
    return request({
      url: `/servicestatistics/statistics/registerCount/${day}`,
      method: 'post'
    });
  },
  //2 获取统计数据
  getDataSta (searchObj) {
    return request({
      url: `/servicestatistics/statistics/showData/${searchObj.type}/${searchObj.begin}/${searchObj.end}`,
      method: 'get'
    });
  }
};