import request from '@/utils/request';

const apiName = '/admin/acl/permission';

export default {
  getNestedTreeList () {
    return request({
      url: `${apiName}`,
      method: 'get'
    });
  },
  removeById (id) {
    return request({
      url: `${apiName}/remove/${id}`,
      method: "delete"
    });
  },
  saveLevelOne (menu) {
    return request({
      url: `${apiName}/save`,
      method: "post",
      data: menu
    });
  },
  update (menu) {
    return request({
      url: `${apiName}/update`,
      method: "put",
      data: menu
    });
  },
  toAssign (roleId) {
    return request({
      url: `${apiName}/toAssign/${roleId}`,
      method: 'get'
    });
  },
  doAssign (roleId, permissionId) {
    return request({
      url: `${apiName}/doAssign`,
      method: "post",
      params: { roleId, permissionId }
    });
  }
};
