import request from '@/utils/request';

export default {
  getPlayAuth (vid) {
    return request({
      url: `/servicevod/vod/getPlayAuth/${vid}`,
      method: 'get'
    });
  }
};