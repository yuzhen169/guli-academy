import request from '@/utils/request';

export default {
  //分页讲师查询的方法
  getPageList (page, limit) {
    return request({
      url: `/serviceedu/teacherFront/getTeacherFrontList/${page}/${limit}`,
      method: 'post'
    });
  },
  //讲师详情的方法
  getTeacherInfo (id) {
    return request({
      url: `/serviceedu/teacherFront/getTeacherFrontInfo/${id}`,
      method: 'get'
    });
  }

};